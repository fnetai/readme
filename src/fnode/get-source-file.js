import fs from 'node:fs';
import path from 'node:path';

export default async ({ file }) => {
    file = file || '.workspace/dist/default/esm/index.js';
    const cwd = process.cwd();
    let sourceFile = path.resolve(cwd, file);
    if (!fs.existsSync(sourceFile)) {
        sourceFile = path.resolve(cwd, 'dist/default/esm/index.js');
        if (!fs.existsSync(sourceFile)) throw new Error('source file not found');
    }

    const content = fs.readFileSync(sourceFile, 'utf8');
        
    return {
        file: file,
        content
    }
}