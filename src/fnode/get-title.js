
import fnetYaml from "@fnet/yaml";

const loadYamlFile = async (filepath) => {
  try {
    const result = await fnetYaml({ file: filepath });
    return result?.parsed;
  } catch (err) {
    console.warn(`Warning: Could not read or parse ${filepath}.`);
    return null;
  }
};

export default async ({ title }) => {
  if (title) return title;

  const project = await loadYamlFile('node.yaml') || await loadYamlFile('flow.yaml');
  if (!project) throw new Error('Could not find node.yaml or flow.yaml');

  title = project['ai::name'] || project['npm::name'] || project['name'] || project['title'];

  return title;
}