import fs from 'node:fs';
import path from 'node:path';

export default async ({ response }) => {
    if (!response) throw new Error('response is required');

    const aiContent = response.choices?.[0].message?.content;

    if (!aiContent) throw new Error('openai response is invalid');

    const targetFile = path.resolve(process.cwd(), 'readme.md');

    fs.writeFileSync(targetFile, aiContent);
}