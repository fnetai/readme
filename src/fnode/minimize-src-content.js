import terser from "terser";

export default async ({ content }) => {

    const terserOptions = {
        mangle: false,
        output: {
            comments: false
        },
        keep_classnames: true,
        keep_fnames: true
    };

    const result = await terser.minify(content, terserOptions);

    return result.code;
}