export default async ({ openai, prompt }) => {

    if (!openai) throw new Error('openai is required');
    if (!prompt) throw new Error('prompt is required');

    const response = await openai.chat.completions.create({
        // model: "gpt-3.5-turbo",
        model: "gpt-4o",
        temperature: 1,
        max_tokens: 1024,
        top_p: 1,
        frequency_penalty: 0,
        presence_penalty: 0,
        ...prompt
    });

    return response;
}