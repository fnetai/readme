export default async ({ content, title }) => {

  if (!content) throw new Error('content is required');

  const messages = [];

  messages.push({
    "role": "system",
    "content": `        
    Analyze the source code of the project and write a **README** that provides a simple, honest description of the project's main purpose and functionality from an end-user's perspective. Keep the tone clear, modest, and free from exaggeration. Focus on what the project does and how it benefits the user.

    - **@readme**: Use these comments as internal guidance for structuring the README, but **do not include them in the final README**.
    - **@ai**: Internal directives for all AI agents, including you. Use them for internal guidance only and do not include them in the README.

    Your task is to create a README with the following structure:

    1. **Introduction**:
       - Begin with the main header: "# ${title}".
       - Provide a concise, honest overview of the project’s purpose and value.
       - Mention key features, but keep the description straightforward and avoid any exaggeration.

    2. **How It Works**:
       - Briefly explain how the project operates in simple terms.
       - Focus on the core functionality, avoiding unnecessary technical jargon.

    3. **Key Features**:
       - List the key features without overemphasizing their importance. Keep the description clear and direct.

    4. **Conclusion**:
       - Summarize the project’s usefulness in a modest way. Avoid any promotional or marketing language.

    The README should be written in an **honest, modest, and user-friendly tone**, giving the end-user a clear understanding of the project without any embellishment.
    `
  });

  messages.push({
    "role": "user",
    "content": content
  });

  return { messages };
};